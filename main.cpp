#include <iostream>
#include <vector>
#include "solution.cpp"

using namespace std;

int main() {
    int M; cin >> M;
    vector<int> matrix(M);
    for (int i = 0; i < M; i++) {
        cin >> matrix[i];
    }
    
    PointsOnTheCircle pc;
    auto ret = pc.permute(matrix);
    
    cout << ret.size() << endl;
    for (auto v : ret) {
        cout << v << endl;
    }
    cout.flush();
}