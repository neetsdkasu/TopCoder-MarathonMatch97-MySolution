#include <algorithm>
#include <cmath>
#include <iostream>
#include <vector>

#ifdef _MSC_VER
#include <chrono>
inline auto get_time() {
	return std::chrono::system_clock::now();
}
inline auto to_msec(int t) {
	return std::chrono::milliseconds(t);
}
typedef std::chrono::system_clock::time_point Time;
#else
const double ticks_per_sec = 2800000000;
inline double get_time() {
	uint32_t lo, hi;
	asm volatile ("rdtsc" : "=a" (lo), "=d" (hi));
	return (((uint64_t)hi << 32) | lo) / ticks_per_sec;
}
inline double to_msec(int t) {
	return double(t) / 1000.0;
}
typedef double Time;
#endif

using namespace std;

typedef long long int64;
typedef unsigned long long uint64;

uint64 XSFT(88172645463325252ULL);
inline uint64 rnd() {
	XSFT ^= XSFT << 13; XSFT ^= XSFT >> 17;
	return (XSFT ^= XSFT << 5);
}
inline uint64 randNext(uint64 upper) { return rnd() % upper; }
inline uint64 randNext(uint64 lower, uint64 upper)
{ return rnd() % (upper - lower) + lower; }
inline double randNextDouble()
{ return double(rnd()) / double(uint64(-1LL)); }

typedef pair<int, int> Tp2i;
typedef pair<double, int> Tpdi;
typedef vector<vector<int64> > Dist;
typedef vector<int> ivec;
typedef vector<ivec> imat;

const int LargeValue = 10000;

class Changer {
public:
    virtual uint64 calc(ivec&, ivec&, uint64) = 0;
    virtual void revert(ivec&, ivec&) = 0;
    virtual void adapt(ivec&, ivec&) = 0;
};

typedef Changer* PChanger;

class Swap2Vertex : public Changer {
    int N;
    Dist &ds;
    imat &edges;
    int i, j, vi, vj;
public:
    Swap2Vertex(int _N, Dist& _ds, imat &_edges)
        : N(_N), ds(_ds), edges(_edges) {}
    uint64 calc(ivec&, ivec&, uint64);
    void revert(ivec&, ivec&);
    void adapt(ivec&, ivec&);
};

class MoveVertex : public Changer {
    int N;
    Dist &ds;
    vector<Tp2i> &allEdges;
    int i, j, p;
public:
    MoveVertex(int _N, Dist& _ds, vector<Tp2i> &_allEdges)
        : N(_N), ds(_ds), allEdges(_allEdges) {}
    uint64 calc(ivec&, ivec&, uint64);
    void revert(ivec&, ivec&);
    void adapt(ivec&, ivec&);
};

class Permute5Vertex : public Changer {
    int N;
    Dist &ds;
    vector<Tp2i> &allEdges;
    imat permute5s;
    ivec vs, bs;
    int pos;
public:
    Permute5Vertex(int _N, Dist& _ds, vector<Tp2i> &_allEdges)
            : N(_N), ds(_ds), allEdges(_allEdges), vs(5), bs(5) {
        int xs[] = {0, 1, 2, 3, 4};
        while (next_permutation(xs, xs + 5)) {
            vector<int> ys(xs, xs +  5);
            permute5s.push_back(ys);
        }
    }
    uint64 calc(ivec&, ivec&, uint64);
    void revert(ivec&, ivec&);
    void adapt(ivec&, ivec&);
};

class Rotate4Vertex : public Changer {
    int N;
    Dist &ds;
    vector<Tp2i> &allEdges;
    int p0, p1, p2, p3, limit, sw, k;
    ivec indexes;
public:
    Rotate4Vertex(int _N, Dist& _ds, vector<Tp2i> &_allEdges)
            : N(_N), ds(_ds), allEdges(_allEdges)
            , limit(_N - (_N & 3) - 4), k(_N), indexes(_N) {
        for (int i = 0; i < _N; i++) {
            indexes[i] = i;
        }
    }
    void shuffle();
    uint64 calc(ivec&, ivec&, uint64);
    void revert(ivec&, ivec&);
    void adapt(ivec&, ivec&);
};

void force(int, imat&, Dist&, imat&, ivec&, Time, vector<Tp2i>&);
void sim_annealing(int, imat&, Dist&, imat&, ivec&, Time, vector<Tp2i>&);

class PointsOnTheCircle {
public:
    vector<int> permute(vector<int> matrix);
};

vector<int> PointsOnTheCircle::permute(vector<int> matrix) {
    auto time0 = get_time() + to_msec(9800);
    const int N = (int)sqrt(matrix.size());
    
    Dist ds(N);
    const auto PI = 4.0 * atan(1.0);
    for (int i = 0; i < N; i++) {
        ds[i].resize(N);
    }
    for (int i = 0; i < N - 1; i++) {
        for (int j = i + 1; j < N; j++) {
            auto th = PI * double(j - i) / double(N);
            ds[j][i] = ds[i][j] = uint64(2000000.0 * sin(th));
        }
    }
    
    vector<Tp2i> allEdges;
    imat edges(N);
    for (int i = 0; i < N - 1; i++) {
        auto a = i * N;
        for (int j = i + 1; j < N; j++) {
            if (matrix[a + j] == 0) { continue; }
            edges[i].push_back(j);
            edges[j].push_back(i);
            allEdges.push_back(Tp2i{i, j});
        }
    }
    
    imat table(N);
    for (int i = 0; i < N; i++) {
        table[i].resize(N);
        for (int j = 0; j < N; j++) {
            if (matrix[i * N + j]) {
                table[i][j] = 1;
            } else {
                table[i][j] = LargeValue;
            }
        }
    }
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            for (int k = 0; k < N; k++) {
                if (table[j][k] > table[j][i] + table[i][k]) {
                    table[j][k] = table[j][i] + table[i][k];
                }
            }
        }
    }
    
    ivec ret(N);
    for (int i = 0; i < N; ++i) {
        ret[i] = i;
    }
    
    force(N, table, ds, edges, ret, time0, allEdges);
    
    sim_annealing(N, table, ds, edges, ret, time0, allEdges);
    
    return ret;    
}

void force(int N, imat &table, Dist &ds, imat &edges, ivec &ret, Time time0, vector<Tp2i> &allEdges) {
    vector<double> xs(N), ys(N);
    for (int i = 0; i < N; i++) {
        xs[i] = randNextDouble() - 0.5;
        ys[i] = randNextDouble() - 0.5;
    }
    
    int maxT = 0;
    for (int i = 0; i < N - 1; i++) {
        for (int j = i + 1; j < N; j++) {
            int t = table[i][j];
            if (t == LargeValue) { continue; }
            if (t > maxT) { maxT = t; }
        }
    }
    
    vector<double> sqs(maxT + 1);
    for (int i = 1; i <= maxT; i++) {
        sqs[i] = sqrt(double(i)) * 10.0;
    }
    
    const double Gr = 67.0;
    
    for (int k = 0; k <= 100; k++) {
        auto time1 = get_time();
        if (time1 > time0) { break; }
        vector<double> xp(N), yp(N);
        for (int i = 0; i < N - 1; i++) {
            for (int j = i + 1; j < N; j++) {
                int t = table[i][j];
                if (t == LargeValue) { continue; }
                auto dx = xs[j] - xs[i];
                auto dy = ys[j] - ys[i];
                auto r = sqrt(dx * dx + dy * dy);
                if (t == 1) {
                    dx = Gr * dx / r;
                    dy = Gr * dy / r;
                    xp[i] += dx;
                    yp[i] += dy;
                    xp[j] -= dx;
                    yp[j] -= dy;
                } else {
                    auto tq = sqs[t] / r;
                    dx *= tq;
                    dy *= tq;
                    xp[i] -= dx;
                    yp[i] -= dy;
                    xp[j] += dx;
                    yp[j] += dy;
                }
            }
        }
        for (int i = 0; i < N; i++) {
            xs[i] += xp[i];
            ys[i] += yp[i];
        }
    }
    
    vector<Tpdi> ls(N);
    for (int i = 0; i < N; i++) {
        ls[i] = Tpdi(atan2(ys[i], xs[i]), i);
    }
    
    sort(ls.begin(), ls.end());
    
    for (int i = 0; i < N; i++) {
        ret[i] = ls[i].second;
    }
    
}

void sim_annealing(int N, imat &table, Dist &ds, imat &edges, ivec &ret, Time time0, vector<Tp2i> &allEdges) {
    ivec vtx(N), per(N);
    for (int i = 0; i < N; i++) {
        vtx[ret[i]] = i;
        per[i] = ret[i];
    }
    
    uint64 score = 0;
    for (auto it = allEdges.begin(); it != allEdges.end(); it++) {
        score += ds[vtx[it->first]][vtx[it->second]];
    }
    cerr << "first: " << score << endl;
    
    const double Alpha = 0.9999;
    const double MaxTime = 150000000.0;
    const int iMaxTime = int(MaxTime);
    
    uint64 minScore = score;
    bool cs = allEdges.size() < 500;
    
    Swap2Vertex c_s2v(N, ds, edges);
    MoveVertex c_mv(N, ds, allEdges);
    Permute5Vertex c_p5v(N, ds, allEdges);
    Rotate4Vertex c_r4v(N, ds, allEdges);
    
    PChanger chgrs[8];
    chgrs[0] = &c_s2v;
    chgrs[1] = &c_mv;
    chgrs[2] = &c_s2v;
    chgrs[3] = &c_p5v;
    chgrs[4] = &c_s2v;
    chgrs[5] = &c_mv;
    chgrs[6] = &c_r4v;
    chgrs[7] = &c_p5v;
    int ci = 1;
    
    int xx = 0;
    
    for (int lp = 0; lp < iMaxTime; lp++) {
        
        auto time1 = get_time();
        if (time1 > time0) {
            cerr << "Loop: " << lp
                << " (" << (100.0 * double(lp) / MaxTime) << "%)" << endl;
            cerr << "xx: " << xx << endl;
            break;
        }
        auto chgr = chgrs[ci];
        ci = (ci + 1) & 7;
        
        auto tmpScore = chgr->calc(vtx, per, score);
        
        if (tmpScore >= score) {
            auto te = (MaxTime - double(lp) * 10.0) / MaxTime;
            auto r = randNextDouble();
            if (r > te * te * 0.0001) {
                chgr->revert(vtx, per);
                continue;
            }
            xx++;
            if (xx < 10) {
                cerr << "r=" << r
                     << ", te=" << te
                     << ", tsc=" << tmpScore
                     << ", lp=" << lp
                     << endl;
            }
        }
        
        score = tmpScore;
        chgr->adapt(vtx, per);
        
        if (score < minScore) {
            minScore = score;
            copy(per.begin(), per.end(), ret.begin());
        }
    }
    
    cerr << "sc: " << minScore << endl;
}

uint64 Swap2Vertex::calc(ivec &vtx, ivec &per, uint64 score) {
    i = int(randNext(N));
    j = int(randNext(N - 1)) + 1 + i;
    if (j >= N) { j -= N; }
    vi = vtx[i];
    vj = vtx[j];
    for (auto it = edges[i].begin(); it != edges[i].end(); it++) {
        if (*it == j) { continue; }
        int ve = vtx[*it];
        score += ds[ve][vj] - ds[ve][vi];
    }
    for (auto it = edges[j].begin(); it != edges[j].end(); it++) {
        if (*it == i) { continue; }
        int ve = vtx[*it];
        score += ds[ve][vi] - ds[ve][vj];
    }
    return score;
}
void Swap2Vertex::revert(ivec &vtx, ivec &per) {
}
void Swap2Vertex::adapt(ivec &vtx, ivec &per) {
    vtx[i] = vj;
    vtx[j] = vi;
    per[vi] = j;
    per[vj] = i;
}

uint64 MoveVertex::calc(ivec &vtx, ivec &per, uint64 score) {
    i = int(randNext(N));
    p = vtx[i];
    j = int(randNext(N - 1)) + 1 + p;
    if (j >= N) { j -= N; }
    
    int lw = min(p, j);
    int up = p + j - lw;
    uint64 tmpScore = 0;
    bool f = j < p;
    for (auto it = allEdges.begin(); it != allEdges.end(); it++) {
        int t1 = vtx[it->first];
        int t2 = vtx[it->second];
        t1 = (t1 < lw || up < t1)
            ? t1 : (t1 == p ? j
            : (f ? t1 + 1 : t1 - 1));
        t2 = (t2 < lw || up < t2)
            ? t2 : (t2 == p ? j
            : (f ? t2 + 1 : t2 - 1));
        tmpScore += ds[t1][t2];
    }
    return tmpScore;
}
void MoveVertex::revert(ivec &vtx, ivec &per) {
}
void MoveVertex::adapt(ivec &vtx, ivec &per) {
    if (j < p) {
        for (int t = p; t > j; t--) {
            per[t] = per[t - 1];
            vtx[per[t]] = t;
        }
    } else {
        for (int t = p; t < j; t++) {
            per[t] = per[t + 1];
            vtx[per[t]] = t;
        }
    }
    per[j] = i;
    vtx[i] = j;
}

uint64 Permute5Vertex::calc(ivec &vtx, ivec &per, uint64 score) {
    pos = int(randNext(N));
    int pn = int(randNext(permute5s.size()));
    ivec &ws  = permute5s[pn];
    for (int k = 0; k < 5; k++) {
        int j = pos + k;
        if (j >= N) { j -= N; }
        bs[k] = j;
        int t = pos + ws[k];
        if (t >= N) { t -= N; }
        int v = per[j];
        vs[k] = v;
        vtx[v] = t;
    }
    uint64 tmpScore = 0;
    for (auto it = allEdges.begin(); it != allEdges.end(); it++) {
            tmpScore += ds[vtx[it->first]][vtx[it->second]];
    }
    return tmpScore;
}
void Permute5Vertex::revert(ivec &vtx, ivec &per) {
    for (auto it = bs.begin(); it != bs.end(); it++) {
        vtx[per[*it]] = *it;
    }
}
void Permute5Vertex::adapt(ivec &vtx, ivec &per) {
    for (auto it = vs.begin(); it != vs.end(); it++) {
        per[vtx[*it]] = *it;
    }
}

void Rotate4Vertex::shuffle() {
    for (int i = 0; i < N - 1; i++) {
        int j = int(randNext(N - 1 - i)) + 1;
        swap(indexes[i], indexes[j]);
    }
}

uint64 Rotate4Vertex::calc(ivec &vtx, ivec &per, uint64 score) {
    k += 4;
    if (k > limit) {
        shuffle();
        k  = 0;
    }
    p0 = indexes[k];
    p1 = indexes[k + 1];
    p2 = indexes[k + 2];
    p3 = indexes[k + 3];
    sw = vtx[p0];
    vtx[p0] = vtx[p1];
    vtx[p1] = vtx[p2];
    vtx[p2] = vtx[p3];
    vtx[p3] = sw;
    uint64 tmpScore = 0;
    for (auto it = allEdges.begin(); it != allEdges.end(); it++) {
            tmpScore += ds[vtx[it->first]][vtx[it->second]];
    }
    return tmpScore;

}
void Rotate4Vertex::revert(ivec &vtx, ivec &per) {
    vtx[p3] = vtx[p2];
    vtx[p2] = vtx[p1];
    vtx[p1] = vtx[p0];
    vtx[p0] = sw;
}
void Rotate4Vertex::adapt(ivec &vtx, ivec &per) {
    per[vtx[p0]] = p0;
    per[vtx[p1]] = p1;
    per[vtx[p2]] = p2;
    per[vtx[p3]] = p3;
}
