Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp2i = System.Tuple(Of Integer, Integer)
Imports Tpdi = System.Tuple(Of Double, Integer)

Public Class PointsOnTheCircle
	Dim rand As New Random(19831983)
    Const LargeValue As Integer = 100000
	
	Public Function permute(matrix() As Integer) As Integer()
		Dim time0 As Integer = Environment.TickCount + 9800
        Dim N As Integer = CInt(Math.Sqrt(CDbl(matrix.Length)))
		
		Console.Error.WriteLine("{0}, {1}", time0, rand.Next(0, 100))
		
        Dim ds(N - 1, N - 1) As Long
        For i As Integer = 0 To N - 2
            For j As Integer = i + 1 To N - 1
                Dim th As Double = Math.PI * CDbl(j - i) / CDbl(N)
                ds(i, j) = CLng(2000000.0 * Math.Sin(th))
                ds(j, i) = ds(i, j)
            Next j
        Next i
        
        Dim mat(N - 1, N - 1) As Boolean
        Dim allEdges As New List(Of Tp2i)()
        Dim edges(N - 1) As List(Of Integer)
        For i As Integer = 0 To N - 1
            edges(i) = New List(Of Integer)()
        Next i
        For i As Integer = 0 To N - 2
            Dim a As Integer = i * N
            For j As Integer = i + 1 To N - 1
                If matrix(a + j) = 0 Then Continue For
                edges(i).Add(j)
                edges(j).Add(i)
                allEdges.Add(New Tp2i(i, j))
                mat(i, j) = True
                mat(j, i) = True
            Next j
        Next i

        Dim table(N - 1, N - 1) As Integer
        For i As Integer = 0 To N - 1
            For j As Integer = 0 To N - 1
                If mat(i, j) Then
                    table(i, j) = 1
                Else
                    table(i, j) = LargeValue
                End If
            Next j
            table(i, i) = 0
        Next i
        
        For i As Integer = 0 To N - 1
            For j As Integer = 0 To N - 1
                For k As Integer = 0 To N - 1
                    If table(j, k) > table(j, i) + table(i, k) Then
                        table(j, k) = table(j, i) + table(i, k)
                    End If
                Next k
            Next j
        Next i
        
 		Dim ret(N-1) As Integer
        For i As Integer = 0 To N - 1
            ret(i) = i
        Next i
        
        force(N, table, ds, edges, ret, time0, allEdges)
        
        sim_annealing(N, mat, ds, edges, ret, time0, allEdges)
        
		permute = ret
		
    End Function
    
    Private Sub force(N As Integer, table(,) As Integer, _
            ds(,) As Long, edges() As List(Of Integer), _
            ret() As Integer, time0 As Integer, allEdges As List(Of Tp2i))
        
        Dim xs(N - 1) As Double
        Dim ys(N - 1) As Double
        For i As Integer = 0 To N - 1
            xs(i) = rand.NextDouble() - 0.5
            ys(i) = rand.NextDouble() - 0.5
        Next i

        Dim maxT As Integer = 0
        For i As Integer = 0 To N - 2
            For j As Integer = i + 1 To N - 1
                Dim t As Integer = table(i, j)
                If t = LargeValue Then Continue For
                If t > maxT Then maxT = t
            Next j
        Next i
        
        Dim sqs(maxT) As Double
        For i As Integer = 1 To maxT
            sqs(i) = Math.Sqrt(CDbl(i)) * 10.0
        Next i
        
        Dim Gr As Double = rand.NextDouble() * 150.0 + 50.0
        
        For k As Integer = 0 To 100
            Dim time1 As Integer = Environment.TickCount
            If time1 > time0 Then Exit For
            Dim xp(N - 1) As Double
            Dim yp(N - 1) As Double
            For i As Integer = 0 To N - 2
                For j As Integer = i + 1 To N - 1
                    Dim t As Integer = table(i, j)
                    If t = LargeValue Then Continue For
                    Dim dx As Double = xs(j) - xs(i)
                    Dim dy As Double = ys(j) - ys(i)
                    Dim r As Double = Math.Sqrt(dx * dx + dy * dy)
                    If t = 1 Then
                        dx = Gr * dx / r
                        dy = Gr * dy / r
                        xp(i) += dx
                        yp(i) += dy
                        xp(j) -= dx
                        yp(j) -= dy
                    Else
                        Dim tq As Double = sqs(t) / r
                        dx = tq * dx
                        dy = tq * dy
                        xp(i) -= dx
                        yp(i) -= dy
                        xp(j) += dx
                        yp(j) += dy
                    End If
                Next j
            Next i
            For i As Integer = 0 To N - 1
                xs(i) += xp(i)
                ys(i) += yp(i)
            Next i
        Next k
        
        Dim ls(N - 1) As Tpdi
        For i As Integer = 0 To N - 1
            ls(i) = New Tpdi(Math.Atan2(ys(i), xs(i)), i)
        Next i
        Array.Sort(ls)
        
        For i As Integer = 0 To N - 1
            ret(i) = ls(i).Item2
        Next i    
    End Sub

    Private Sub sim_annealing(N As Integer, mat(,) As Boolean, _
            ds(,) As Long, edges() As List(Of Integer), _
            ret() As Integer, time0 As Integer, allEdges As List(Of Tp2i))

		Dim vtx(N - 1) As Integer
		Dim per(N - 1) As Integer
        For i As Integer = 0 To N - 1
            vtx(ret(i)) = i
            per(i) = ret(i)
        Next i
    
        Dim score As Long = 0
        For Each tp As Tp2i In allEdges
            score += ds(vtx(tp.Item1), vtx(tp.Item2))
        Next tp
        Console.Error.WriteLine("first: {0}", score)
        
        Const Alpha As Double = 0.9999
        Const MaxTime As Double = 15000000.0
        Dim minScore As Long = score
        Dim cs As Boolean = allEdges.Count < 500
        
        Dim c_s2v As New Swap2Vertex(N, ds, edges, rand)
        Dim c_mv As New MoveVertex(N, ds, allEdges, rand)
        Dim c_p5v As New Permute5Vertex(N, ds, allEdges, rand)
        Dim c_r4v As New Rotate4Vertex(N, ds, allEdges, rand)
        
        Dim chgrs() As Changer = { _
            c_s2v, c_mv, c_s2v, c_p5v, c_s2v, c_mv, c_r4v, c_p5v _
            }
        Dim ci As Integer = 0
        
        For lp As Integer = 0 To CInt(MaxTime)
        
            Dim time1 As Integer = Environment.TickCount
            If time1 > time0 Then
                Console.Error.WriteLine("Loop: {0} ({1}%)", lp, 100.0 * CDbl(lp) / MaxTime)
                Exit For
            End If
            
            Dim chgr As Changer = chgrs(ci)
            ci = (ci + 1) And 7
            
            Dim tmpScore As Long = chgr.Calc(vtx, per, score)

            If tmpScore >= score Then
                Dim te As Double = Math.Pow(Alpha, CDbl(lp) / MaxTime)
                Dim ee As Double = If(cs, _
                    Math.Exp(100.0 * CDbl(minScore - tmpScore) / te / CDbl(tmpScore)), _
                    Math.Exp(CDbl(minScore - tmpScore) / te))
                If rand.NextDouble() > ee Then
                    chgr.Revert(vtx, per)
                    Continue For
                End If
            End If
            
            score = tmpScore
            chgr.Adapt(vtx, per)
            
            If score < minScore Then
                minScore = score
                Array.Copy(per, ret, ret.Length)
            End If
            
        Next lp
        
        Console.Error.WriteLine("sc: {0}", minScore)
        
	End Sub
    
    Interface Changer
        Function Calc(vtx() As Integer, per() As Integer, score As Long) As Long
        Sub Revert(vtx() As Integer, per() As Integer)
        Sub Adapt(vtx() As Integer, per() As Integer)
    End Interface

    Class Swap2Vertex
        Implements Changer
        Dim N As Integer
        Dim ds(,) As Long
        Dim edges() As List(Of Integer)
        Dim rand As Random
        Dim i As Integer, j As Integer, vi As Integer, vj As Integer
        Sub New(N As Integer, ds(,) As Long, edges() As List(Of Integer), rand As Random)
            Me.N = N
            Me.ds = ds
            Me.edges = edges
            Me.rand = rand
        End Sub
        Function Calc(vtx() As Integer, per() As Integer, score As Long) As Long Implements Changer.Calc
            i = rand.Next(N)
            j = rand.Next(N - 1) + 1 + i
            If j >= N Then j -= N
            vi = vtx(i)
            vj = vtx(j)
            For Each e As Integer In edges(i)
                If e = j Then Continue For
                Dim ve As Integer = vtx(e)
                score += ds(ve, vj) - ds(ve, vi)
            Next e
            For Each e As Integer In edges(j)
                If e = i Then Continue For
                Dim ve As Integer = vtx(e)
                score += ds(ve, vi) - ds(ve, vj)
            Next e
            Calc = score
        End Function
        Sub Revert(vtx() As Integer, per() As Integer) Implements Changer.Revert
        End Sub
        Sub Adapt(vtx() As Integer, per() As Integer) Implements Changer.Adapt
            vtx(i) = vj
            vtx(j) = vi
            per(vi) = j
            per(vj) = i
        End Sub
    End Class

    Class MoveVertex
        Implements Changer
        Dim N As Integer
        Dim ds(,) As Long
        Dim allEdges As List(Of Tp2i)
        Dim rand As Random
        Dim i As Integer, j As Integer, p As Integer
        Sub New(N As Integer, ds(,) As Long,allEdges As List(Of Tp2i), rand As Random)
            Me.N = N
            Me.ds = ds
            Me.allEdges = allEdges
            Me.rand = rand
        End Sub
        Function Calc(vtx() As Integer, per() As Integer, score As Long) As Long Implements Changer.Calc
            i = rand.Next(N)
            p = vtx(i)
            j = rand.Next(N - 1) + 1 + p
            If j >= N Then j -= N
            
            Dim lw As Integer = Math.Min(p, j)
            Dim up As Integer = p + j - lw
            Dim tmpScore As Long = 0
            For Each e As Tp2i In allEdges
                Dim t1 As Integer = vtx(e.Item1)
                Dim t2 As Integer = vtx(e.Item2)
                t1 = If(t1 < lw OrElse up < t1, t1, If(t1 = p, j, If(j < p, t1 + 1, t1 - 1)))
                t2 = If(t2 < lw OrElse up < t2, t2, If(t2 = p, j, If(j < p, t2 + 1, t2 - 1)))
                tmpScore += ds(t1, t2)
            Next e
            Calc = tmpScore
        End Function
        Sub Revert(vtx() As Integer, per() As Integer) Implements Changer.Revert
        End Sub
        Sub Adapt(vtx() As Integer, per() As Integer) Implements Changer.Adapt
            If j < p Then
                For t As Integer = p To j + 1 Step -1
                    per(t) = per(t - 1)
                    vtx(per(t)) = t
                Next t
            Else 
                For t As Integer = p To j - 1
                    per(t) = per(t + 1)
                    vtx(per(t)) = t
                Next t
            End If
            per(j) = i
            vtx(i) = j
        End Sub
    End Class
    
    Class Permute5Vertex
        Implements Changer
        Dim N As Integer
        Dim ds(,) As Long
        Dim allEdges As List(Of Tp2i)
        Dim permute5s As List(Of Integer())
        Dim rand As Random
        Dim vs(4) As Integer, bs(4) As Integer, pos As Integer
        Sub New(N As Integer, ds(,) As Long, allEdges As List(Of Tp2i), rand As Random)
            Me.N = N
            Me.ds = ds
            Me.allEdges = allEdges
            Me.rand = rand
            permute5s = New List(Of Integer())()
            Dim xs() As Integer = {0, 1, 2, 3, 4}
            Do
                Dim p As Integer = - 1
                For i As Integer = UBound(xs) To 1 Step -1
                    If xs(i - 1) < xs(i) Then
                        p = i
                        Exit For
                    End If
                Next i
                If p < 0 Then Exit Do
                Dim lw As Integer = p
                Dim up As Integer = UBound(xs)
                Do While lw < up
                    Dim swp As Integer = xs(lw)
                    xs(lw) = xs(up): xs(up) = swp
                    lw += 1: up -= 1
                Loop
                Dim x As Integer = xs(p - 1)
                For i As Integer = p To UBound(xs)
                    If xs(i) > x Then
                        xs(p - 1) = xs(i)
                        xs(i) = x
                        Exit For
                    End If
                Next i
                permute5s.Add(TryCast(xs.Clone(), Integer()))
            Loop            
        End Sub
        Function Calc(vtx() As Integer, per() As Integer, score As Long) As Long Implements Changer.Calc
            pos = rand.Next(N)
            Dim ws() As Integer = permute5s(rand.Next(permute5s.Count))
            For k As Integer = 0 To 4
                Dim j As Integer = pos + k
                If j >= N Then j -= N
                bs(k) = j
                Dim t As Integer = pos + ws(k)
                If t >= N Then t -= N
                Dim v As Integer = per(j)
                vs(k) = v
                vtx(v) = t
            Next k
            Dim tmpScore As Long = 0
            For Each e As Tp2i In allEdges
                tmpScore += ds(vtx(e.Item1), vtx(e.Item2))
            Next e
            Calc = tmpScore
        End Function
        Sub Revert(vtx() As Integer, per() As Integer) Implements Changer.Revert
            For Each j As Integer In bs
                vtx(per(j)) = j
            Next j
        End Sub
        Sub Adapt(vtx() As Integer, per() As Integer) Implements Changer.Adapt
            For Each v As Integer In vs
                per(vtx(v)) = v
            Next v
        End Sub
    End Class

    Class Rotate4Vertex
        Implements Changer
        Dim N As Integer
        Dim ds(,) As Long
        Dim allEdges As List(Of Tp2i)
        Dim rand As Random
        Dim p0 As Integer, p1 As Integer, p2 As Integer, p3 As Integer
        Dim limit As Integer, indexes() As Integer, sw As Integer, k As Integer
        Sub New(N As Integer, ds(,) As Long,allEdges As List(Of Tp2i), rand As Random)
            Me.N = N
            Me.ds = ds
            Me.allEdges = allEdges
            Me.rand = rand
            limit = N - (N And 3) - 4
            ReDim indexes(N - 1)
            For i As Integer = 0 To N - 1
                indexes(i) = i
            Next i
            k = N
        End Sub
        Sub Shuffle()
            For i As Integer = 0 To N - 2
                Dim j As Integer = rand.Next(N - 1 - i) + 1
                Dim swp As Integer = indexes(i)
                indexes(i) = indexes(j)
                indexes(j) = swp
            Next i
        End Sub
        Function Calc(vtx() As Integer, per() As Integer, score As Long) As Long Implements Changer.Calc
            k += 4
            If k > limit Then
                Shuffle()
                k = 0
            End If
            p0 = indexes(k)
            p1 = indexes(k + 1)
            p2 = indexes(k + 2)
            p3 = indexes(k + 3)
            sw = vtx(p0)
            vtx(p0) = vtx(p1)
            vtx(p1) = vtx(p2)
            vtx(p2) = vtx(p3)
            vtx(p3) = sw
            Dim tmpScore As Long = 0
            For Each e As Tp2i In allEdges
                tmpScore += ds(vtx(e.Item1), vtx(e.Item2))
            Next e
            Calc = tmpScore
        End Function
        Sub Revert(vtx() As Integer, per() As Integer) Implements Changer.Revert
            vtx(p3) = vtx(p2)
            vtx(p2) = vtx(p1)
            vtx(p1) = vtx(p0)
            vtx(p0) = sw
        End Sub
        Sub Adapt(vtx() As Integer, per() As Integer) Implements Changer.Adapt
            per(vtx(p0)) = p0
            per(vtx(p1)) = p1
            per(vtx(p2)) = p2
            per(vtx(p3)) = p3
        End Sub
    End Class
    
End Class