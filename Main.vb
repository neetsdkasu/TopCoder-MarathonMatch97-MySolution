Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

	Public Sub Main()
		Try
			Dim M As Integer = CInt(Console.ReadLine())
			Dim matrix(M - 1) As Integer
			For i As Integer = 0 To UBound(matrix)
				matrix(i) = CInt(Console.ReadLine())
			Next i
			
			Dim pc As New PointsOnTheCircle()
			Dim ret() As Integer = pc.permute(matrix)

			Console.WriteLine(ret.Length)
			For Each r As Integer In ret
				Console.WriteLine(r)
			Next r
			Console.Out.Flush()
			
		Catch Ex As Exception
			Console.Error.WriteLine(ex)
		End Try
	End Sub

End Module